﻿// first capital letter(4)
// use special character
// give digit
using System.Text.RegularExpressions;

namespace Assignment_PasswordValidation
{
    internal class PasswordValidate
    {
        public const string pattern = @"^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#&()–[{}]:;',?/*~$^+=<>])$";

        public static bool pattern_search(string num)
        {
            if (num != null)
                return Regex.IsMatch(num, pattern);
            else  return false;
        }
    }
}
